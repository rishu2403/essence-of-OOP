from collections import defaultdict
from typing import List
from webdev import WebDeveloper
from de import DataEngineer
from ds import DataScientist

class Manager:
    def __init__(self, web_devs: List[WebDeveloper], data_engineers: List[DataEngineer], data_scientists: List[DataScientist]) -> None:
        self.web_devs = web_devs
        self.data_engineers = data_engineers
        self.data_scientists = data_scientists

    def get_all_progress(self):
        progress = defaultdict(list)      
        for wd in self.web_devs:
            progress['WebDeveloper'].append(wd.get_progress())
        for de in self.data_engineers:
            progress['DataEngineer'].append(de.get_progress())
        for ds in self.data_scientists:
            progress['DataScientist'].append(ds.get_progress())
        return progress.copy()

    def make_report(self):
        """
        Collects progresses from all the employees, and adds some custom attributes.
        """
        progress_report = self.get_all_progress()
        progress_report['start_time'] = '1-Feb-2022'
        progress_report['end_time'] = '2-Feb-2022'
        return progress_report
