from base import Employee

class WebDeveloper(Employee):

    def get_progress(self):
        return 'WebDev Task done'
