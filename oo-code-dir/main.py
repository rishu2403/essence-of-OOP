from manager import Manager
from webdev import WebDeveloper
from de import DataEngineer
from ds import DataScientist

if __name__ == "__main__":
    w1 = WebDeveloper()
    w2 = WebDeveloper()
    de1 = DataEngineer()
    de2 = DataEngineer()
    ds1 = DataScientist()
    m = Manager([w1, w2, de1, de2, ds1])
    current_progress = m.get_all_progress()
    print(current_progress)
