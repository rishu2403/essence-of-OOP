from collections import defaultdict
from typing import List
from base import Employee

class Manager:
    def __init__(self, employees: List[Employee]) -> None:
        self.employees = employees

    def get_all_progress(self):
        report = defaultdict(list)
        for employee in self.employees:
            report[employee.get_type()].append(employee.get_progress())
        return report.copy()

    def make_report(self):
        """
        Collects progresses from all the employees, and adds some custom attributes.
        """
        progress_report = self.get_all_progress()
        progress_report['start_time'] = '1-Feb-2022'
        progress_report['end_time'] = '2-Feb-2022'
        return progress_report
