import abc


class Employee(abc.ABC):

    @classmethod
    def get_type(cls) -> str:
        return cls.__name__

    @abc.abstractmethod
    def get_progress(self) -> str:
        pass
    
    # @abc.abstractmethod
    # def get_num_hours(self) -> str:
    #     pass


# class Employee:
    
#     def get_type(self) -> str:
#         return self.__class__.__name__

#     def get_progress(self) -> str:
#         raise NotImplementedError
