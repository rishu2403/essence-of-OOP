from base import Employee

class DataScientist(Employee):

    def get_progress(self):
        return 'DS Task done'
